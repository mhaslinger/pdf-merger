﻿namespace PDFMerger
{
    public class PDFFile
    {
        public int Position { get; set; }
        public string Name { get; set; }
        public string FullPath { get; set; }
    }
}