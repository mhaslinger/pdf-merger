﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Elysium.Controls;
using iTextSharp.text;
using iTextSharp.text.exceptions;
using iTextSharp.text.pdf;
using Microsoft.Win32;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace PDFMerger
{
    public partial class MainWindow
    {
        private ObservableCollection<PDFFile> _files;

        public MainWindow()
        {
            this.InitializeComponent();
            this._setupMain();
            this._setupLicense();
            this._setupAbout();
            _setupHelp();
        }

        private void _setupHelp()
        {
            HelpTextBlock.Text = Res.Help_Text;
        }

        private void _setupAbout()
        {
            AboutTextBlock.Text = Res.About_Text;
        }

        private void _setupMain()
        {
            this._files = new ObservableCollection<PDFFile>();
            this.FileListView.ItemsSource = this._files;
        }

        private void _setupLicense()
        {
            if (File.Exists("License.txt"))
            {
                var licFile = File.ReadAllText("License.txt");
                this.LicenseTextBlock.Text = licFile;
            }
            else
            {
                MessageBox.Show(Res.Lic_Missing_Text);
            }
        }

        private void GoToCodeButtonClick(object sender, RoutedEventArgs e)
        {
            Process.Start(new ProcessStartInfo(Res.Code_Url));
            e.Handled = true;
        }

        private void ButtonHighlightMouseEnter(object sender, MouseEventArgs e)
        {
            var button = (Button) sender;
            button.Background = new SolidColorBrush(Colors.White);
            button.Foreground = new SolidColorBrush(Colors.Black);
            button.BorderBrush = new SolidColorBrush(Colors.White);
        }

        private void SuccessButtonMouseLeave(object sender, MouseEventArgs e)
        {
            var button = (Button) sender;
            button.Background = new SolidColorBrush(Colors.Green);
            button.Foreground = new SolidColorBrush(Colors.White);
            button.BorderBrush = new SolidColorBrush(Colors.Green);
        }

        private void DangerButtonMouseLeave(object sender, MouseEventArgs e)
        {
            var button = (Button) sender;
            button.Background = new SolidColorBrush(Colors.DarkRed);
            button.Foreground = new SolidColorBrush(Colors.White);
            button.BorderBrush = new SolidColorBrush(Colors.DarkRed);
        }

        private static string _showSelectDestinationFileDialogue()
        {
            var curDir = Environment.CurrentDirectory;
            var dlg = new CommonOpenFileDialog
                      {
                          Title = "Select File Destination Folder",
                          IsFolderPicker = true,
                          InitialDirectory = curDir,
                          AddToMostRecentlyUsedList = false,
                          AllowNonFileSystemItems = false,
                          DefaultDirectory = curDir,
                          EnsureFileExists = true,
                          EnsurePathExists = true,
                          EnsureReadOnly = false,
                          EnsureValidNames = true,
                          Multiselect = false,
                          ShowPlacesList = true
                      };
            if (dlg.ShowDialog() != CommonFileDialogResult.Ok)
            {
                return null;
            }
            var folder = dlg.FileName;
            return !string.IsNullOrWhiteSpace(folder) ? folder : null;
        }

        private static IEnumerable<string> _showAddFilesDialogue()
        {
            var curDir = Environment.CurrentDirectory;
            var dlg = new OpenFileDialog {Multiselect = true, InitialDirectory = curDir, DefaultExt = "*.pdf", Filter = "PDF Files (*.pdf)|*.pdf|All Files (*.*)|*.*"};
            var result = dlg.ShowDialog();
            if (!(result ?? false))
            {
                return null;
            }
            return dlg.FileNames;
        }

        private void AddButtonClick(object sender, RoutedEventArgs e)
        {
            var files = _showAddFilesDialogue();
            if (files == null)
            {
                return;
            }
            foreach (var file in from filePath in files
                                 let fileInfo = new FileInfo(filePath)
                                 orderby fileInfo.Name
                                 select new PDFFile {FullPath = filePath, Name = fileInfo.Name})
            {
                this._addFile(file);
            }
        }

        private void _addFile(PDFFile file)
        {
            var pos = _files.Count + 1;
            file.Position = pos;
            _files.Add(file);
        }

        private void RemoveButtonClick(object sender, RoutedEventArgs e)
        {
            var file = this._getSelectedFile();
            if (file == null)
            {
                return;
            }
            var idx = file.Position - 1;
            this._files.RemoveAt(idx);
            _updateFilePositions();
        }

        private PDFFile _getSelectedFile()
        {
            var temp = FileListView.SelectedItem;
            if (temp != null)
            {
                return (PDFFile) temp;
            }
            return null;
        }

        private async void MergeButtonClick(object sender, RoutedEventArgs e)
        {
            this._showProgressOverlay();
            this._addProgressMessage("starting merge process...");
            var files = _files.Select(f => f.FullPath).ToList();
            var dest = DestinationPathTextBox.Text;
            var destFileName = OutputFileNameTextBox.Text;
            var exit = false;
            if (files.Count < 2)
            {
                MessageBox.Show("You have to add at least two files.");
                exit = true;
            }
            if (string.IsNullOrWhiteSpace(dest) || !Directory.Exists(dest))
            {
                MessageBox.Show("Destination invalid");
                exit = true;
            }
            if (!exit)
            {
                if (string.IsNullOrWhiteSpace(destFileName))
                {
                    destFileName = "MergedPDF" + Guid.NewGuid() + ".pdf";
                }
                if (!destFileName.EndsWith(".pdf"))
                {
                    destFileName += ".pdf";
                }
                var outputStream = await _mergeFiles(files);
                await _writeOutputFile(outputStream, dest, destFileName);
                this._addProgressMessage("progress complete!");
            }
            ProgressOverlayCloseButton.Visibility = Visibility.Visible;
            ProgressOverlayProgressRing.State = ProgressState.Indeterminate;
            ProgressOverlayCloseButton.Visibility = Visibility.Visible;
        }

        private async Task _writeOutputFile(Stream outputStream, string path, string fileName)
        {
            outputStream.Position = 0;
            var fullPath = path + "/" + fileName;
            if (File.Exists(fullPath))
            {
                var messageBoxResult = MessageBox.Show("File Exists", "Override file?", MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.No)
                {
                    return;
                }
                File.Delete(fullPath);
            }
            using (var fs = new FileStream(fullPath, FileMode.OpenOrCreate, FileAccess.Write))
            {
                try
                {
                    this._addProgressMessage("starting to write output file...");
                    await outputStream.CopyToAsync(fs);
                    this._addProgressMessage("output file written");
                }
                catch
                {
                }
                finally
                {
                    outputStream.Dispose();
                }
            }
        }

        private async Task<MemoryStream> _mergeFiles(IEnumerable<string> inputFilePaths)
        {
            var files = (from f in inputFilePaths
                         where File.Exists(f)
                         select f).ToList();
            var outStream = new MemoryStream();
            Document document = null;
            _addProgressMessage("starting to merge files...");
            try
            {
                var reader = new PdfReader(files.ElementAt(0));               
                document = new Document(reader.GetPageSizeWithRotation(1));
                var writer = new PdfCopy(document, outStream);
                document.Open();
                _addPages(reader.NumberOfPages, reader, writer);
                var cnt = 2;
                foreach (var reader2 in files.Skip(1).Select(pdfPath => new PdfReader(pdfPath)))
                {
                    await _addPagesAsync(reader2.NumberOfPages, reader2, writer);
                    this._addProgressMessage("merged " + cnt + " of " + files.Count + " files");
                    cnt++;
                }
            }
            catch (InvalidPdfException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (document != null)
                {
                    document.Close();
                }
            }
            return new MemoryStream(outStream.ToArray());
        }

        private static Task _addPagesAsync(int pages, PdfReader reader, PdfCopy writer)
        {
            return Task.Run(() => _addPages(pages, reader, writer));
        }

        private static void _addPages(int pages, PdfReader reader, PdfCopy writer)
        {
            for (var i = 1; i < pages + 1; i++)
            {
                var page = writer.GetImportedPage(reader, i);
                writer.AddPage(page);
            }
        }

        private void SelectDestButtonClick(object sender, RoutedEventArgs e)
        {
            var dest = _showSelectDestinationFileDialogue();
            if (!string.IsNullOrWhiteSpace(dest))
            {
                DestinationPathTextBox.Text = dest;
            }
        }

        private void MoveDownButtonClick(object sender, RoutedEventArgs e)
        {
            var file = this._getSelectedFile();
            if (file == null || file.Position == _files.Count)
            {
                return;
            }
            this._files.RemoveAt(file.Position - 1);
            this._files.Insert(file.Position, file);
            this._updateFilePositions();
        }

        private void MoveUpButtonClick(object sender, RoutedEventArgs e)
        {
            var file = this._getSelectedFile();
            if (file == null || file.Position <= 1)
            {
                return;
            }
            this._files.RemoveAt(file.Position - 1);
            this._files.Insert(file.Position - 2, file);
            this._updateFilePositions();
        }

        private void _clearOverlayText()
        {
            ProgressOverlayText.Text = "";
        }

        private void _hideProgressOverlay()
        {
            ProgressOverlayGrid.Visibility = Visibility.Hidden;
            this._clearOverlayText();
        }

        private void _showProgressOverlay()
        {
            ProgressOverlayProgressRing.State = ProgressState.Busy;
            ProgressOverlayCloseButton.Visibility = Visibility.Hidden;
            ProgressOverlayGrid.Visibility = Visibility.Visible;
        }

        private void _addProgressMessage(string message)
        {
            var sb = new StringBuilder(ProgressOverlayText.Text);
            sb.Append(DateTime.Now.ToString("HH:mm:ss"));
            sb.Append(": ");
            sb.Append(message);
            sb.Append("\n");
            ProgressOverlayText.Text = sb.ToString();
            ProgressOverlayScrollViewer.ScrollToBottom();
        }

        private void FileListViewDrop(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                return;
            }
            var files = (string[]) e.Data.GetData(DataFormats.FileDrop);
            foreach (var file in from filePath in files
                                 let fileInfo = new FileInfo(filePath)
                                 where fileInfo.Extension.Equals(".pdf")
                                 orderby fileInfo.Name
                                 select new PDFFile {FullPath = filePath, Name = fileInfo.Name})
            {
                this._addFile(file);
            }
        }

        private void _updateFilePositions()
        {
            for (var i = 0; i < _files.Count; i++)
            {
                _files[i].Position = i + 1;
            }
            FileListView.Items.Refresh();
        }

        private void ProgressOverlayCloseButtonClick(object sender, RoutedEventArgs e)
        {
            this._hideProgressOverlay();
        }

        private void ClearButtonClick(object sender, RoutedEventArgs e)
        {
            _files.Clear();
            FileListView.Items.Refresh();
        }

        private void FileListViewPreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            FileListScrollViewer.ScrollToVerticalOffset(FileListScrollViewer.VerticalOffset - e.Delta);
            e.Handled = true;
        }
    }
}